import { combineReducers } from "redux";
import products from "./Category-reducer";

export default combineReducers({
  products
});