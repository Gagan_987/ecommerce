import React from 'react'
import {Link} from 'react-router-dom'
import {FaShoppingCart} from "react-icons/fa";
import {AiFillHeart} from "react-icons/ai";
import {FaSearch} from "react-icons/fa";
import {BiMenu} from 'react-icons/bi'

class Header extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            value : ""
        }
    }
    handleChange = (event) => {
        this.setState({value: event.target.value});
        console.log(event.target.value)
      }
    
    render(){
        return(
            <div className="header">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                           <div className="header-left-box">
                           <Link to="/" className="logo-box"><img src="images/logo.png" alt="ecom logo" className="img-fluid" /><span>ecom</span></Link>
                           <Link to="#" className="menu-icon" onClick={this.Menubar}><span><BiMenu /></span></Link>
                           </div>
                        </div>
                        <div className="col-md-6">
                            <div className="header-right-box">
                                <div className="search-box">
                                    <FaSearch />
                                    <input type="text" value={this.state.value} placeholder="search product" onChange={this.handleChange} />
                                </div>
                                <div className="header-icon">
                                    <Link to="/" ><span><FaShoppingCart /></span></Link>
                                    <Link to="/" ><span><AiFillHeart /></span></Link>
                                    <Link to="/" ><span className="lang-box">aA</span></Link>
                                    <Link to="/"><span><img src="images/user.png" alt="user img" className="user-img"/></span></Link>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Header;