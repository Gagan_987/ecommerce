import React from 'react'
import {Link} from 'react-router-dom'
import {  ImInstagram } from "react-icons/im";
import {FaFacebookF} from "react-icons/fa";
import {FaTwitter} from "react-icons/fa";
import {AiFillYoutube } from "react-icons/ai"

class Footer extends React.Component{
    render(){
        return(
            <div className="footer">
                <div className="container">
                    <div className="inner-footer">
                        <div className="row">
                            <div className="col-md-3">
                                <div className="left-footer">
                                    <Link to="/" className="logo">E-commerce</Link>
                                    <p>+912 2222 1111</p>
                                    <Link to="/" className="mail">sales@ae.com</Link>
                                </div>
                            </div>
                            <div className="col-md-3">

                            </div>
                            <div className="col-md-3">
                                <div className="inner-footer-icon">
                                    <div className="footer-icon">
                                        <Link to="/"><ImInstagram /></Link>
                                        <Link to="/"><FaFacebookF /></Link>
                                        <Link to="/"><FaTwitter /></Link>
                                        <Link to="/"><AiFillYoutube /></Link>
                                    </div>
                                    <p>social media</p>
                                </div>
                            </div>
                            <div className="col-md-3">
                               <div className="right-inner-footer">
                                    <ul>
                                        <li><Link to="#">About</Link></li>
                                        <li><Link to="#">Policies</Link></li>
                                        <li><Link to="#">Contact Us</Link></li>
                                    </ul>
                               </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <p className="text-right">Copyrights &#169; 2020 ecom All Rights Reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Footer;