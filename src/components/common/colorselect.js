import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220,
  },
  selectEmpty: {
  },
}));

export default function SimpleSelectOne() {
  const classes = useStyles();
  const [color, setAge] = React.useState('');

  const handleChange = (event) => {
    setAge(event.target.value);
  };
  return (
    <div>
      <FormControl variant="outlined" className={classes.formControl} >
      <InputLabel id="demo-simple-select-filled-label">Select Color</InputLabel>
      <p style={{ display: 'none' }}>{color}</p>
        <Select
          onChange={handleChange}
        >
          <MenuItem value="black">black</MenuItem>
          <MenuItem value="white">white</MenuItem>
          <MenuItem value="green">green</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}

