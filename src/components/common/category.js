import React from 'react'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'
import {getCategoriesApi} from './../../api/categoryApi'
// import { fetchProducts } from "./../../action/categoryAction";


  export default class Category extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            Category : [
                {
                    
                    name : "Home",
                    subCategories : [],
                    navlinks : "/product-details"
                },
                {
                    
                    name : "Top wear",
                    subCategories : [],
                    navlinks : "/product-details"
                },
                {
                    
                    name : "Bottom wear",
                    subCategories : [],
                    navlinks : "/product-details"
                },
                {
                    
                    name : "Footwear",
                    subCategories : [],
                    navlinks : "/product-details"
                },
                {
                   
                    name : "fashion",
                    subCategories : [],
                    navlinks : "/product-details"
                },
                {
                    
                    name : "Watches",
                    subCategories : [],
                    navlinks : "/product-details"
                },
                {
        
                    name : "Gadgets",
                    subCategories : [],
                    navlinks : "/product-details"
                }
            ]
        }
    }

    componentDidMount(){
        // this.props.dispatch(fetchProducts());
        getCategoriesApi('5f73ea190104e9079ad749ea', 'landing')
        .then(res => {
            // console.log(res);
            this.setState({ Category: res.getCategories.list })
            
        })
        .catch(e => console.log(e));

        
        
    }
    render(){
        return(
            <div className="category category2">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                           <div className="sub-box">
                            <ul className="sub-cat-item">  
                            {
                                    this.state.Category.map(function(Category , Sublist){
                                            return <Navbar key={Category} Nav={Category} />
                                            
                                    })
                                    
                                }
                            </ul>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
// const mapStateToProps = (state) =>{
//     return{
//         myCategory : state.Category
//     }
// }

// const mapDispatchToProps = (dispatch) =>{
//     return{
//         ChangeName : () =>{dispatch({anotherName()})}
//     }
// }

// export default connect(mapStateToProps)(Category);



class Navbar extends React.Component{ 

    render(){    
        return(
                <li ><Link to="/" className="nav-list">{this.props.Nav.name}</Link>
                    <ul className="sub-box-cat">
                        {
                            this.props.Nav.subCategories.map(function(Subcategory , sublink){
                            return <Sub key={sublink} navy={Subcategory} />
                            })
                        }
                    </ul>   
                </li>
        )
    }
}

class Sub extends React.Component{
    
    render(){
        return(
        <li><Link to="/productlist">{this.props.navy.name}</Link></li>
        )
    }
}