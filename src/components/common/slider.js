import React from "react";
import Slider from "react-slick";
 
class SimpleSlider extends React.Component {
  render() {
    var settings = {
      dots: false,
      autoplay :true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <Slider {...settings}>
        <div className="banner-slider-box">
            <img src="images/slide.png" alt="slider 1" />
        </div>
        <div className="banner-slider-box">
            <img src="images/slide.png" alt="slider 2"/>
        </div>
        <div className="banner-slider-box">
            <img src="images/slide.png" alt="slider 3"/>
        </div>
        <div className="banner-slider-box">
            <img src="images/slide.png" alt="slider 4"/>
        </div>
      </Slider>
    );
  }
}
export default SimpleSlider;