import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220,
  },
  selectEmpty: {
  },
}));

export default function SimpleSelectTwo() {
  const classes = useStyles();
  const [size, setAge] = React.useState('');

  const handleChange = (event) => {
    setAge(event.target.value);
  };
  return (
    <div>
      <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel id="demo-simple-select-filled-label">Select Size</InputLabel>
      <p style={{ display: 'none' }}>{size}</p>
        <Select
          onChange={handleChange}
        >
          <MenuItem value="X">X</MenuItem>
          <MenuItem value="M">M</MenuItem>
          <MenuItem value="L">L</MenuItem>
          <MenuItem value="Xl">XL</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}
