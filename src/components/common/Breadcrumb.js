import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import {Breadcrumbs as MUIbreadcrumbs , Link , Typography} from '@material-ui/core'
import {withRouter} from 'react-router-dom'


const useStyles = makeStyles((theme) => ({
    root: {
      '& > * + *': {
        marginTop: theme.spacing(2),
      },
    },
  }));

const Breadcrumbs = props =>{
    const classes = useStyles();
    const {history , location : {pathname}} = props;
    const pathnames = pathname.split("/").filter(x => x);
    return(
        <MUIbreadcrumbs className={classes.root}>
            <Link onClick={() => history.push("/")} className="bread-one">Home</Link>
            {
                pathnames.map((name , index) =>{
                    const routeTo = `/${pathnames.slice(0 , index+1).join("/")}`
                return <Link onClick={()=>history.push(routeTo)} className="bread-two">{name}</Link>
                })
            }
        </MUIbreadcrumbs>
    )
}
 export default withRouter(Breadcrumbs);
