import React, { Component } from 'react';
import ReactSlick from './ReactSlick';
import '../styles/examples.css';

export default class ReactSlickExample extends Component {
    render() {
        return (
            <div className="fluid react-slick">
                <div className="fluid__image-container">
                    <ReactSlick {...{
                        rimProps: {
                            enlargedImagePortalId: 'portal',
                            enlargedImageContainerDimensions: {
                                width: '200%',
                                height: '100%'
                            }
                        }
                    }}/>
                </div>
                <div className="fluid__instructions" style={{position: 'relative'}}>
                    <div className="mag-box">
                        <div
                            id="portal"
                            className="portal"
                        />
                    </div>
                </div>
            </div>
        );
    }
}
