import React from 'react'
import {Link} from 'react-router-dom'
import {AiFillHeart} from "react-icons/ai";
import {FaPlus} from 'react-icons/fa';
import H3 from './../heading/heading_h3';

export default class TrendProduct extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            Service : [
                {
                    productImg : "images/products.png",
                    name : <H3 title = "marty's top 1" />,
                    HeartSign : <AiFillHeart />,
                    alt : "product 1",
                    price : "4.50"
                },
                {
                    productImg : "images/products.png",
                    name : <H3 title = "marty's top 2" />,
                    HeartSign : <AiFillHeart />,
                    alt : "product 2",
                    price : "4.50"
                },
                {
                    productImg : "images/products.png",
                    name : <H3 title = "marty's top 3" />,
                    HeartSign : <AiFillHeart />,
                    alt : "product 3",
                    price : "4.50"
                },
                {
                    productImg : "images/products.png",
                    name : <H3 title = "marty's top 4" />,
                    HeartSign : <AiFillHeart />,
                    alt : "product 4",
                    price : "4.50"
                }
            ]
        }
    }
    render(){
        return(
            <div className="trend-product">
                <div className="container">
                    <div className="row">
                        {
                            this.state.Service.map(function (Service , ServiceList) {
                                return <Items key={ServiceList} link={Service } />
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }
}

class Items extends React.Component{

    render(){
        return(
            <div className="col-md-3">
                <div className="product-box">   
                        <div className="product-upbox">
                        <Link to="/productDetail"><img src={this.props.link.productImg} alt={this.props.link.alt}/></Link>
                        <Link to="/productDetail" className="heart-box"> <span>{this.props.link.HeartSign}</span></Link>
                        </div>
                        <div className="product-downbox">
                        <div className="inner-downbox">{this.props.link.name}  </div>
                            <p>KD {this.props.link.price}</p>
                        </div>
                    
                </div>
            </div>
        )
    }
}
