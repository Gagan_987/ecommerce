import React from 'react'

export default function H1(props) {
return <h1 className={" heading_h1 " +  props.className}>{props.title ? props.title : props.children}</h1>
}