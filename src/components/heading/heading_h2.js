import React from 'react'

export default function H2(props) {
return <h2 className={" heading_h2 " +  props.className}>{props.title ? props.title : props.children}</h2>
}