import React from 'react'

export default function H3(props) {
return <h3 className={" heading_h3 " +  props.className}>{props.title ? props.title : props.children}</h3>
}