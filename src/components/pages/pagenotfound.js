import React from 'react'


class PageNotFound extends React.Component{
    render(){
        return(
            <div className="page-not-found">
                <h2>page not found</h2>
            </div>
        )
    }
}
export default PageNotFound;