import React from 'react'
import  TrendProduct from './../common/trend-product'
import Category from './../common/category'
import  NewArrival from './../common/new-arrival'
import H2 from './../heading/heading_h2'
import SimpleSlider from './../common/slider'
import $ from 'jquery';

class Home extends React.Component{
    componentDidMount(){
        this.jquery();
    }
    jquery(){
        $(".heart-box").click(function(){
            $(this).toggleClass("heart-box-in")
        })
    }
    render(){
        return(
            <div className="home">
                <div className="banner-slider"><SimpleSlider /></div>
                <Category   />
                <div className="container">
                    <H2 title="trending products" />
                </div>
                <TrendProduct />
                <div className="container">
                <H2 title="new arrivals" className="arrive-title"/>
                </div>
                <NewArrival />
            </div>
        )
    }
}
export default Home;