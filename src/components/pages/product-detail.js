import React from 'react'
import {Link} from 'react-router-dom'
import Category from './../common/category'
import H2 from './../heading/heading_h2'
import H1 from './../heading/heading_h1'
import Breadcrumbs from './../common/Breadcrumb'
import TrendProduct from './../common/trend-product'
import SimpleSelectOne from './../common/colorselect'
import SimpleSelectTwo from './../common/sizeselect'
import {getProductDetailsApi} from './../../api/getproductDetailsApi'
import ReactSlickExample from './../common/product-detail-slider'
import $ from "jquery";

class ProductDetails extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            ProductDetails : [

            ]
        }
    }

    componentDidMount(){
        getProductDetailsApi('5f73ea190104e9079ad749ea',  '5f8d962fa4d73107f37b384f')
        .then(res => {
            console.log(res);
            this.setState({ ProductDetails: res.getProductDetails })        
        })
        .catch(e => console.log(e));
        this.jquery();
        
    }

    jquery(){
        $(".select-size-color a").click(function(){
            $(this).toggleClass("heartcolor");
        });
        
        $(".fluid__image-container").hover(function(){
            $('html, body').animate({
                scrollTop: $("#pro-box1").offset().top
            }, 500);
            $(".portal").toggleClass("magnify-img")
            // $("#root").toggleClass("overchange")
        })
    }
    render(){
        return(
                <div className="product-detail">
                       <Category />
                       <div className="container">
                       <Breadcrumbs />
                            <div className="slider-box">
                            <div className="row">
                            <div className="col-md-5" id="pro-box1">
                                <div className="slider-content">
                                    <ReactSlickExample />
                                </div>
                            </div>
                            <div className="col-md-7" id="pro-box2">
                               <div className="product-content">
                                <H1 title="Printed neck lightweight T-shirt" />
                                    <p className="brand">Wrogn</p>
                                    <span className="color-title">Product details</span>
                                    <p className="pro-detail">off-white and green printed T-shirt with ombre-dyed effect,has a round neck, short sleeves</p>
                                    <span className="color-title">select color</span>
                                    <div className="select-color-box select-color-boxes">
                                        <SimpleSelectOne />
                                    </div>
                                    <span className="color-title">select size</span>
                                    <div className="select-color-box select-size-color">
                                    <SimpleSelectTwo />
                                    </div>
                                    <p className="product-price">kd<span> 80.0 </span> <span><del>kd 80.0</del></span> <span>[20% off]</span></p>
                                    <div className="btn-box">
                                        <Link to="#">Add to bag</Link>
                                        <Link to="#">Buy Now</Link>
                                    </div>
                               </div>
                            </div>
                       </div>
                            </div>
                       <div className="similar-product">
                            <div className="inner-similar-title">
                            <H2 title="similar products" className="htitle"/>
                            <Link to="#" className="show-btn">show all</Link>
                            </div>
                            <TrendProduct />
                            
                       </div>
                    </div>
                </div>
        )
    }
}
export default ProductDetails;