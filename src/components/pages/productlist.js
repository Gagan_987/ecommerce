import React from 'react'
import {Link} from 'react-router-dom'
import Category from './../common/category'
import NewArrival from './../common/new-arrival'
import Breadcrumbs from './../common/Breadcrumb'
import {getProductApi} from './../../api/getproductApi'
import H2 from './../heading/heading_h2'
import {AiFillHeart} from "react-icons/ai";
import {FaPlus} from 'react-icons/fa';
import {ImCross} from 'react-icons/im';

 class ProductList extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            Product : [
                    {
                        productName : "dfasdk",
                        productNameInArabic : "",
                        price : 4.50,
                        quantity : "",
                        storeId : "",
                        weight :"",
                        description : "",
                        productImg: "./images/products.png",
                        alt:"sdklfjsd",
                        HeartSign : <AiFillHeart />,

                    },
                    {
                        productName : "",
                        productNameInArabic : "",
                        price : "",
                        quantity : "",
                        storeId : "",
                        weight :"",
                        description : "",
                        productImg: "./images/products.png",
                        alt:"dsklfsdjhf"
                    }

            ]
        }
    }

    componentDidMount(){
        getProductApi('5f73ea190104e9079ad749ea',  '' , '')
        .then(res => {
            // console.log(res);
            this.setState({ Product: res.getProducts.list })        
        })
        .catch(e => console.log(e));


        
        
    }
    render(){
        return(
            <div className="productlist">
                <Category />
                <div className="container">
                    <Breadcrumbs />
                    { <div className="row product-min-box">
                    {
                                    this.state.Product.map(function(Product , Sublist){
                                            return <Navbars key={Product.productName} Navvy={Product} />
                                            
                                    })
                                    
                                }
                    </div>}
                </div>
                {/* <NewArrival /> */}
                <div>

                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) =>{
    return{
        myname : state.name
    }
}

export default ProductList;

class Navbars extends React.Component{
    render(){
        return(
            <div className="col-md-3">
                <div className="product-box">   
                        <div className="product-upbox">
                        <Link to="/productDetail"><img src={this.props.Navvy.productImg} alt={this.props.Navvy.alt}/></Link>
                        <Link to="/" className="heart-box"><span>{this.props.Navvy.HeartSign}</span></Link>
                        </div>
                        <div className="product-downbox">
                        <div className="inner-downbox">{this.props.Navvy.productName}</div>
                            <p>KD {this.props.Navvy.price}</p>
                        </div>
                    
                </div>
            </div>
        )
    }
}