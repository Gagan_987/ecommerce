import {getCategoriesApi} from './../api/categoryApi'
export const FETCH_PRODUCTS_BEGIN   = 'FETCH_PRODUCTS_BEGIN';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_FAILURE = 'FETCH_PRODUCTS_FAILURE';


export const fetchProductsBegin = () => ({
  type: FETCH_PRODUCTS_BEGIN
});

export const fetchProductsSuccess = products => ({
  type: FETCH_PRODUCTS_SUCCESS,
  payload: { products }
});

export const fetchProductsFailure = error => ({
  type: FETCH_PRODUCTS_FAILURE,
  payload: { error }
});





export function fetchProducts() {
  return dispatch => {
    dispatch(fetchProductsBegin());
    return getCategoriesApi('5f73ea190104e9079ad749ea', 'landing')
    .then(res => {
        // console.log(res);
        this.setState({ Category: res.getCategories.list })
        
    })
    .catch(e => console.log(e));return 
  };
}

// Handle HTTP errors since fetch won't.
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}