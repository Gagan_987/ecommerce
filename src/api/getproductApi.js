import { GRAPH_QL } from '../utilities/helpers';

export const getProductApi = (storeId, search , categoryId) => {
  const query_string = `
  query {
    getProducts(
      storeId:"${storeId}"
      search:"${search}"
      categoryId:"${categoryId}"
    ) {
       list{
        _id
        productName
        productNameInArabic
        storeId
        quantity
        price
        weight
        description
      }
    }
  }`;
  return GRAPH_QL(query_string);
};