import { GRAPH_QL } from '../utilities/helpers';

export const getCategoriesApi = (storeId, type) => {
  const query_string = `
  query {
    getCategories(
      storeId:"${storeId}"
      type:"${type}"
    ) {
      list {
        _id
        name
        nameInArabic
        image
        subCategories {
          _id
          name
          nameInArabic
          image
        }
      }
    }
  }`;
  return GRAPH_QL(query_string);
};
