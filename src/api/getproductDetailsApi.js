import { GRAPH_QL } from '../utilities/helpers';

export const getProductDetailsApi = (storeId, _id ,) => {
  const query_string = `
  query {
    getProductDetails(
      storeId:"${storeId}"
      _id:"${_id}"
    ) {
          _id
        productName
        productNameInArabic
        storeId
        quantity
        price
        weight
        description
        variant{
          _id
          name
          nameInArabic
          type
          isQuantity
        }
    }
  }`;
  return GRAPH_QL(query_string);
};