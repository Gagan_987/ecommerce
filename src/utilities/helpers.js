import axios from 'axios';

const ACCESS_TOKEN_KEY = 'ACCESS_TOKEN_KEY';
const LOG = true;
const BASE_URL = 'https://ecom.io/api/v1/customer';

export const GRAPH_QL = (query_string) => {
  return new Promise((resolve, reject) => {
    const token = localStorage.getItem(ACCESS_TOKEN_KEY);
    axios({
      url: BASE_URL,
      headers: {
        accessToken: `Bearer ${token}`,
      },
      method: 'post',
      data: { query: query_string },
    })
    .then(res => {
      if (res.data.data) {
        resolve(res.data.data);
      } else {
        reject(res);
      }
    })
    .catch(e => handleError(e, reject));
  });
};

function handleError(e, reject) {
  if (LOG) {
    console.log(e.response);
  }
  reject(e);
}
