import React ,{useEffect} from 'react';
import './assets/css/style.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
// import Cart from './components/pages/cart'
import Header from './components/includes/header'
import Footer from './components/includes/footer'
import Home from './components/pages/home'
import PageNotFound from './components/pages/pagenotfound'
import ProductDetails from './components/pages/product-detail'
import Productlist from './components/pages/productlist'
import {BrowserRouter as Router , Route, Switch} from 'react-router-dom'
import $ from 'jquery'

function App(props) {


  useEffect(() => {
    $(".menu-icon").click(function(){
      $(".category").toggleClass("menu-content")
     });

   

  },[]);
  
  return (
    <div className="App">
        <Router>
          <Header />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route  path="/productDetail" component={ProductDetails} />
            <Route  path="/productlist" component={Productlist} />
            <Route path="" component={PageNotFound} />
            {/* <Route path="/cart" component={Cart} /> */}
          </Switch>
          <Footer />
        </Router>
    </div>
  );
}

export default App;
